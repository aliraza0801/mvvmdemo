﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using appcompat = Android.Support.V7.Widget;
using MvvmDemo.Core.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace MvvmDemo.Views
{
    [Activity(Label = "Main Activity", MainLauncher = true, Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class FirstView : MvxAppCompatActivity<FirstViewModel>
    {
        internal appcompat.Toolbar toolbar;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.first);
            toolbar = FindViewById<appcompat.Toolbar>(Resource.Id._toolbar);

            
            this.SetSupportActionBar(toolbar);
            this.SupportActionBar.Title = "Tip Calculator";
            if (bundle == null)
                ViewModel.ShowFirstScreen();


        }
        //public override bool OnOptionsItemSelected(IMenuItem item)
        //{
        //    if (SupportFragmentManager.BackStackEntryCount == 0)
        //    {
        //        return base.OnOptionsItemSelected(item);
        //    }

        //    SupportFragmentManager.PopBackStack();
        //    return true;
        //}
        //public override void OnBackPressed()
        //{
            
        //        base.OnBackPressed();
          
        //       // FragmentManager.PopBackStack();
            


        //}
    }
}