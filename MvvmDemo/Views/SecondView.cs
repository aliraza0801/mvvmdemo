﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views.Attributes;
using MvvmDemo.Core.ViewModels;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V4;

namespace MvvmDemo.Views
{
    [MvxFragmentPresentation(typeof(FirstViewModel), Resource.Id.content_frame, true)]
    [Register("mvvmDemo.views.SecondView")]
    public class SecondView : MvxFragment<SecondViewModel>, View.IOnClickListener
    {
        private FirstView mainActivity;
        protected int FragmentId => Resource.Layout.fragment_SecondView;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
            var x = base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(FragmentId, null);

            if (((FirstView)Activity) != null)
            {
                mainActivity = (FirstView)Activity;
                mainActivity.SupportActionBar.Title = "This is Second Screen.";
                mainActivity.SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.back);
                mainActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                mainActivity.toolbar.SetNavigationOnClickListener(this);
            }
                return view;
        }

        public void OnClick(View view)
        {
            mainActivity.SupportActionBar.Title = "First Screen";
            mainActivity.SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.back);
            mainActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(false);
            mainActivity.toolbar.SetNavigationOnClickListener(null);
            this.ViewModel.ShowPrevious();
        }

        
    }
}