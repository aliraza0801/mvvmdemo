﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmDemo.Core.ViewModels
{
   public class FirstViewModel:MvxViewModel
    {
        public void ShowFirstScreen()
        {
            ShowViewModel<TipViewModel>();
        }
    }
}
