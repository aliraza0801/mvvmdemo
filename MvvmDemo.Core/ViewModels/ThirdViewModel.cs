﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MvvmDemo.Core.ViewModels
{
    public class ThirdViewModel : MvxViewModel
    {
        public void ShowPrevious()
        {
            ShowViewModel<SecondViewModel>();
        }

        
    }
}
