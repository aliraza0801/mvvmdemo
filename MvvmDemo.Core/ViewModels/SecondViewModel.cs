﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MvvmDemo.Core.ViewModels
{
   public class SecondViewModel:MvxViewModel
    {
        public void ShowPrevious()
        {
            ShowViewModel<TipViewModel>();
        }
        public ICommand DoSomething
        {
            get
            {
                return new MvxCommand(() =>
                {
                    try
                    {
                        ShowViewModel<ThirdViewModel>();
                        //Close(this);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }

                });
            }
        }
    }
}
